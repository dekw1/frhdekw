import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {LayoutComponent} from './components/layout/layout.component';
import {WorkspaceComponent} from './components/workspace/workspace.component';

@NgModule({
  declarations: [
    LayoutComponent,
    WorkspaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [LayoutComponent]
})
export class AppModule {}
