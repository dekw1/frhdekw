import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {WorkspaceComponent} from './components/workspace/workspace.component';

const ROUTES: Routes = [
  {
    path: '',
    component: WorkspaceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
