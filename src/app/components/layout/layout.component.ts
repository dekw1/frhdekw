import {Component} from '@angular/core';

@Component({
  selector: 'frhdekw-root',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.less']
})
export class LayoutComponent {}
