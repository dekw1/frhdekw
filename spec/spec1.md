# FRHDekw spec v1

## Interface

- 3 text areas always visible, to have distinct workspaces
- An easy way to switch from one to another
- Tabs to group features of the same type
    - Generate code
    - Transform existing code
- In each tab, a folding menu for each feature to show/hide the parameters and the buttons to launch the process

## Acceptation rules

- If you fold a feature or change tab, you shouldn't lose your feature parameters and folding status
